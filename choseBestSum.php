<?php

function chooseBestSum($t, $k, $ls){
    $finalResults = [];
    $sums = [];
    $results = array(array( ));
    foreach ($ls as $element)
        foreach ($results as $combination){
            if(count($combination) > $k){
                continue;
            }else{
                array_push($results, array_merge(array($element), $combination));
                if(count(array_merge(array($element), $combination)) == $k){
                    array_push($finalResults, array_merge(array($element), $combination));
                }
            }
        }
    foreach($finalResults as $results){
        $sum = array_sum($results);
        if($sum <= $t)
            array_push($sums, $sum);
    }
    if($sums)
        return max($sums);
    return null;
}
