<?php

function sqInRect(int $lng, int $wdth) {
    $arr = array();

    if ($lng == $wdth){
        return null;
    }

    while ($lng != $wdth){
        if ($wdth > $lng){
            array_push($arr, $lng);
            $wdth = $wdth - $lng;
        }elseif ($wdth < $lng){
            array_push($arr, $wdth);
            $lng = $lng - $wdth;
        }
    }

    array_push($arr, $wdth);

    return $arr;
}
